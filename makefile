
ENVIRONMENT = development
PROJECT_NAME = rentalhouse
PYTHON_PATH = python3
VENV_PATH = ../venv

######

default:
	$(MAKE) run

install:
	virtualenv -p $(PYTHON_PATH) $(VENV_PATH)
	( \
		. $(VENV_PATH)/bin/activate; \
		pip install -r requirements.txt; \
		python manage.py makemigrations; \
		python manage.py migrate; \
		python manage.py loaddata initial.json; \
		python manage.py loaddata flatpages.json; \
	)

run:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py runserver 4322; \
	)

makemigrations:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py makemigrations; \
	)

migrate:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py migrate; \
		python manage.py loaddata initial.json; \
	)

upgrade:
	( \
		. $(VENV_PATH)/bin/activate; \
		pip install -r requirements.txt; \
	)

test:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py test --failfast --noinput; \
	)

cleandata:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py flush; \
	)

flatpages:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py loaddata flatpages.json; \
	)

syncdb:
	( \
		. $(VENV_PATH)/bin/activate; \
		python manage.py syncdb; \
	)
	
