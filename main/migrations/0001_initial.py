# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('producer', models.CharField(max_length=30)),
                ('engine_capacity', models.DecimalField(decimal_places=2, max_digits=6)),
                ('engine_horsepower', models.DecimalField(decimal_places=2, max_digits=6)),
                ('seats_quantity', models.IntegerField()),
                ('doors_quantity', models.IntegerField()),
                ('load_capacity', models.IntegerField()),
                ('vin_number', models.CharField(max_length=17, unique=True)),
                ('rent_price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('is_available', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='CarAccessory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=30, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='CarType',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=30, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('phone_number', models.CharField(max_length=12, unique=True)),
                ('pesel', models.CharField(max_length=12, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='EngineType',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=30, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Rent',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('rent_cost', models.DecimalField(decimal_places=2, max_digits=8)),
                ('car', models.ForeignKey(to='main.Car')),
                ('client', models.ForeignKey(to='main.Client')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='car_accessories',
            field=models.ManyToManyField(to='main.CarAccessory'),
        ),
        migrations.AddField(
            model_name='car',
            name='car_type',
            field=models.ForeignKey(to='main.CarType'),
        ),
        migrations.AddField(
            model_name='car',
            name='engine_type',
            field=models.ForeignKey(to='main.EngineType'),
        ),
    ]
