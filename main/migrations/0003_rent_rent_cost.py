# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150420_1315'),
    ]

    operations = [
        migrations.AddField(
            model_name='rent',
            name='rent_cost',
            field=models.DecimalField(max_digits=8, decimal_places=2, default=1000),
            preserve_default=False,
        ),
    ]
