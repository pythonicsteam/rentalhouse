# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='car',
            old_name='rent_price',
            new_name='rent_cost',
        ),
        migrations.RemoveField(
            model_name='rent',
            name='rent_cost',
        ),
    ]
