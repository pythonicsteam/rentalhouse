# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20150427_1203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='car',
            name='is_available',
        ),
    ]
