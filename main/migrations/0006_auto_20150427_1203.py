# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20150427_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rent',
            name='client',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
