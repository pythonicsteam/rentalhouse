# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_rent_rent_cost'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='production_year',
            field=models.IntegerField(default=2000),
            preserve_default=False,
        ),
    ]
