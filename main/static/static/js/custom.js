$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#cars .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#cars .item').removeClass('list-group-item');$('#cars .item').addClass('grid-group-item');});

    $('.datepicker').datetimepicker({
        format: 'YYYY-M-D',
        minDate: Date.now()
    });
});