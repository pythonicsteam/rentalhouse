from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.flatpages import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^', include('main.urls', namespace='main')),
    url(r'^about/$', views.flatpage, {'url': '/about/'}, name='about'),
]