RentalHouse
***********

RentalHouse is a web application for car rental company. It comes with clean, elegant design, ready-to-go user system, flexible car categories and many more. It's also super-easy to start with - there is a demo provided (example usage). 

Installation
============

In order for installation to run python3-dev package (or alike) is required. Also virtualenv is needed.
To install this project on a Linux machine simply enter the `rentalhouse` directory and type in the console::

	make install

You may need to adjust paths in a ``makefile``.

Usage
=====

Project is available locally on port 4322.

Administration
--------------

To manage the project use auto-generated admin account::

	L: admin
	P: admin

Link to administrative panel is provided after loging in.

Tests
-----

Project comes with a complete test To run the tests, simply type in the terminal::

	make test

Changes
=======


v1.0
----

- Performed final cleanup.

v0.9
----

- Rating system: users in their rent history panel have the possibility of rating cars that they have already used. Also, in detailed view of car average rating is shown.
- Improved comments look and ordering (now newest first).
- Added filtering by rating.
- Removed car category dropdown (as it's in filtering).

v0.8
----

- Added posibility to cancel rent 3 days prior to starting date.
- Comment system with two statuses of comments: question and 'answer' for people that did not use certain car and for those that had used it prior to commenting.

v0.7
----

- Improved filtering. It's possible to filter by name, car type, engine type, price range, availability dates and accessories.

v0.6
----

- Changed date input in rent form to datepicker.
- Fixed 'Go back' button issues.
- Merged intentional flatpages and added 'About'.
- Translated initial fixture to English.
- Added comments and rating to model.
- First glance at advanced filtering.
- Added button that leads to admin panel.
- Added rent history.

v0.5
----

- Thumbnails for cars. Each car can now have its own image.
- Complete version of project design (frontend).
- Database fixtures (example data to populate the database along with image files for thumbnails).
- Full test suite for the project (you can run the tests by executing ``make test``).

v0.4
----

- First version of project design (frontend). Used Twitter Bootstrap and Django templates.
- Additional functionality (test + view + url pattern + template + templatetag).
	- List of all available categories.
	- Filtering cars by category.

v0.3
----

- Basic functionality (test + view + url pattern + template + form).
	- List of all cars (with pagination).
	- Separate page for each car with detailed information.
	- Order system. Each car can be now purchased for a specified date.
- Test suite for existing functionality (you can run the tests by executing ``make test``).

v0.2
----

- Complex user system for clients integrated with:
	- Client table from model.
	- Django default user system.
	- External auth package ``django-allauth``.
- Admin panel.

v0.1
----

- Complete model (database schema), including tables for:
	- Client.
	- Car.
	- CarType (Category).
	- EngineType.
	- CarAccessory.
	- Rent.
- Project base.
	- Django empty project.
	- Project settings.
	- Git repository.
	- Virtualenv.
	- Set of branches (master, development, ...).
	- Makefile with shortcuts for common commands. Most important ones are:
		- ``make (run)`` (start the project).
		- ``make install`` (install the project: set the database, load fixtures, download and install dependencies, set the virtualenv and more).
		- ``make test`` (run all tests for the project).
		- ``make upgrade`` (upgrade the project dependencies).
