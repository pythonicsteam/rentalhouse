***
# Meetings journal
***
## Meeting 1. May 27, 2015
The aim of the first meeting was to discuss the main concept of the project after taking it from other group.
### Basic informations
The most imporant issue this week is to run the project on all local machines, that now works only on the half. The main conpept of working and coopereation will be the same as in the first part of the project.
### Version project and tasks
We want to have the 0.6 version of the project unil the next meeting.
Tasks will be devided as follows:

* Monika - implementation classes needed for model, creating intro data.

* Marcin - continuation working with tests, creating the link for administrator panel, customisation view with bootstrap.

* Konrad - renting car - handlng possible errors(exceptions), choosing date of rent - datepickers, date validation.

* Tadek - creating flat pages (v0.6), reminding password - mail sending. 

###Next appointment
The second appointment was settled for June 3.
***
## Meeting 2. June 3, 2015
Having run project on local machines we decided to develop 0.7 version of the project deviding tasks as follows:

* Monika - implementation of comment system.

* Marcin - continuation working with tests, taking responsibilities of a Quality Assurance - taking care of code reviews. 

* Konrad - searching cars - implementing and extending criteria

* Tadek - showing current and recently rented cars including prices for those cars. Create view that will facilitate future rented cars voting.

###Next appointment
The second appointment was settled for June 10.
***
## Meeting 3. June 10, 2015
Next part will be creating voting system and comments versions 0.8 and 0.9

Tasks for this week:

* Monika - implementation of comment system.

* Marcin - continuation working with tests, administration panel exchanging django look. 

* Konrad - searching cars - better look, enabling resignation of rent.

* Tadek - rating/voting system + applying flatpages to database
###Next appointment
Next appointment will be pronounced after meeting Client/Lecturer

***
## Meeting 4. June 17, 2015
Waiting for classes that didn't actually take place, we had a meeting.
We talked over the project, last bugs and visual improvements and divide extra part of tasks as follows:

* Monika - improvement of comments system's look.

* Marcin - continuation working with tests, finding unneeded pieces of text

* Tadek - rating/voting system - scripts improvement + better place for average rating, blocking possibility of canceling the car rent by url.
